const data = [{
        name: "Casino Penthouse",
        ipl_name: "vw_casino_penthouse",
        position: {
            X: 976.6364,
            Y: 70.29476,
            Z: 115.1641
        },
        category: "Penthouse"
    },

    {
        name: "Casino Window",
        ipl_name: "vw_casino_penthouse",
        position: {
            X: 976.6364,
            Y: 70.29476,
            Z: 115.1641
        },
        category: "Penthouse"
    },

    {
        name: "Casino Parkhouse",
        ipl_name: "vw_casino_carpark",
        position: {
            X: 1380.0000,
            Y: 200.0000,
            Z: -50.0000
        },
        category: "Penthouse"
    },

    {
        name: "Casino Garage",
        ipl_name: "vw_casino_garage",
        position: {
            X: 1295.0000,
            Y: 230.0000,
            Z: -50.0000
        },
        category: "Garage"
    },

    {
        name: "Casino Main",
        ipl_name: "vw_casino_main",
        position: {
            X: 1100.0000,
            Y: 220.0000,
            Z: -50.0000
        },
        category: "Casino"
    },

    {
        name: "ZancudoBunker",
        ipl_name: "gr_case10_bunkerclosed",
        position: {
            X: -3058.714,
            Y: 3329.19,
            Z: 12.5844
        },
        category: "Bunker"
    },

    {
        name: "Route68Bunker",
        ipl_name: "gr_case9_bunkerclosed",
        position: {
            X: 24.43542,
            Y: 2959.705,
            Z: 58.35517
        },
        category: "Bunker"
    },

    {
        name: "OilfieldsBunker",
        ipl_name: "gr_case3_bunkerclosed",
        position: {
            X: 481.0465,
            Y: 2995.135,
            Z: 43.96672
        },
        category: "Bunker"
    },

    {
        name: "DesertBunker",
        ipl_name: "gr_case0_bunkerclosed",
        position: {
            X: 848.6175,
            Y: 2996.567,
            Z: 45.81612
        },
        category: "Bunker"
    },

    {
        name: "SmokeTreeBunker",
        ipl_name: "gr_case1_bunkerclosed",
        position: {
            X: 2126.785,
            Y: 3335.04,
            Z: 48.21422
        },
        category: "Bunker"
    },

    {
        name: "ScrapyardBunker",
        ipl_name: "gr_case2_bunkerclosed",
        position: {
            X: 2493.654,
            Y: 3140.399,
            Z: 51.28789
        },
        category: "Bunker"
    },

    {
        name: "GrapeseedBunker",
        ipl_name: "gr_case5_bunkerclosed",
        position: {
            X: 1823.961,
            Y: 4708.14,
            Z: 42.4991
        },
        category: "Bunker"
    },

    {
        name: "PalletoBunker",
        ipl_name: "gr_case7_bunkerclosed",
        position: {
            X: -783.0755,
            Y: 5934.686,
            Z: 24.31475
        },
        category: "Bunker"
    },

    {
        name: "Route1Bunker",
        ipl_name: "gr_case11_bunkerclosed",
        position: {
            X: -3180.466,
            Y: 1374.192,
            Z: 19.9597
        },
        category: "Bunker"
    },

    {
        name: "FarmhouseBunker",
        ipl_name: "gr_case6_bunkerclosed",
        position: {
            X: 1570.372,
            Y: 2254.549,
            Z: 78.89397
        },
        category: "Bunker"
    },

    {
        name: "RatonCanyonBunker",
        ipl_name: "gr_case4_bunkerclosed",
        position: {
            X: -391.3216,
            Y: 4363.728,
            Z: 58.65862
        },
        category: "Bunker"
    },

    {
        name: "Modern 1 Apartment",
        ipl_name: "apa_v_mp_h_01_a",
        position: {
            X: -786.8663,
            Y: 315.7642,
            Z: 217.6385
        },
        category: "Online apartament"
    },

    {
        name: "Modern 2 Apartment",
        ipl_name: "apa_v_mp_h_01_c",
        position: {
            X: -786.9563,
            Y: 315.6229,
            Z: 187.9136
        },
        category: "Online apartament"
    },

    {
        name: "Modern 3 Apartment",
        ipl_name: "apa_v_mp_h_01_b",
        position: {
            X: -774.0126,
            Y: 342.0428,
            Z: 196.6864
        },
        category: "Online apartament"
    },

    {
        name: "Mody 1 Apartment",
        ipl_name: "apa_v_mp_h_02_a",
        position: {
            X: -787.0749,
            Y: 315.8198,
            Z: 217.6386
        },
        category: "Online apartament"
    },

    {
        name: "Mody 2 Apartment",
        ipl_name: "apa_v_mp_h_02_c",
        position: {
            X: -786.8195,
            Y: 315.5634,
            Z: 187.9137
        },
        category: "Online apartament"
    },

    {
        name: "Mody 3 Apartment",
        ipl_name: "apa_v_mp_h_02_b",
        position: {
            X: -774.1382,
            Y: 342.0316,
            Z: 196.6864
        },
        category: "Online apartament"
    },

    {
        name: "Vibrant 1 Apartment",
        ipl_name: "apa_v_mp_h_03_a",
        position: {
            X: -786.6245,
            Y: 315.6175,
            Z: 217.6385
        },
        category: "Online apartament"
    },

    {
        name: "Vibrant 2 Apartment",
        ipl_name: "apa_v_mp_h_03_c",
        position: {
            X: -786.9584,
            Y: 315.7974,
            Z: 187.9135
        },
        category: "Online apartament"
    },

    {
        name: "Vibrant 3 Apartment",
        ipl_name: "apa_v_mp_h_03_b",
        position: {
            X: -774.0223,
            Y: 342.1718,
            Z: 196.6863
        },
        category: "Online apartament"
    },

    {
        name: "Sharp 1 Apartment",
        ipl_name: "apa_v_mp_h_04_a",
        position: {
            X: -787.0902,
            Y: 315.7039,
            Z: 217.6384
        },
        category: "Online apartament"
    },

    {
        name: "Sharp 2 Apartment",
        ipl_name: "apa_v_mp_h_04_c",
        position: {
            X: -787.0155,
            Y: 315.7071,
            Z: 187.9135
        },
        category: "Online apartament"
    },

    {
        name: "Sharp 3 Apartment",
        ipl_name: "apa_v_mp_h_04_b",
        position: {
            X: -773.8976,
            Y: 342.1525,
            Z: 196.6863
        },
        category: "Online apartament"
    },

    {
        name: "Monochrome 1 Apartment",
        ipl_name: "apa_v_mp_h_05_a",
        position: {
            X: -786.9887,
            Y: 315.7393,
            Z: 217.6386
        },
        category: "Online apartament"
    },

    {
        name: "Monochrome 2 Apartment",
        ipl_name: "apa_v_mp_h_05_c",
        position: {
            X: -786.8809,
            Y: 315.6634,
            Z: 187.9136
        },
        category: "Online apartament"
    },

    {
        name: "Monochrome 3 Apartment",
        ipl_name: "apa_v_mp_h_05_b",
        position: {
            X: -774.0675,
            Y: 342.0773,
            Z: 196.6864
        },
        category: "Online apartament"
    },

    {
        name: "Seductive 1 Apartment",
        ipl_name: "apa_v_mp_h_06_a",
        position: {
            X: -787.1423,
            Y: 315.6943,
            Z: 217.6384
        },
        category: "Online apartament"
    },

    {
        name: "Seductive 2 Apartment",
        ipl_name: "apa_v_mp_h_06_c",
        position: {
            X: -787.0961,
            Y: 315.815,
            Z: 187.9135
        },
        category: "Online apartament"
    },

    {
        name: " Seductive 3 Apartment",
        ipl_name: "apa_v_mp_h_06_b",
        position: {
            X: -773.9552,
            Y: 341.9892,
            Z: 196.6862
        },
        category: "Online apartament"
    },

    {
        name: " Regal 1 Apartment",
        ipl_name: "apa_v_mp_h_07_a",
        position: {
            X: -787.029,
            Y: 315.7113,
            Z: 217.6385
        },
        category: "Online apartament"
    },

    {
        name: "Regal 2 Apartment",
        ipl_name: "apa_v_mp_h_07_c",
        position: {
            X: -787.0574,
            Y: 315.6567,
            Z: 187.9135
        },
        category: "Online apartament"
    },

    {
        name: "Regal 3 Apartment",
        ipl_name: "apa_v_mp_h_07_b",
        position: {
            X: -774.0109,
            Y: 342.0965,
            Z: 196.6863
        },
        category: "Online apartament"
    },

    {
        name: "Aqua 1 Apartment",
        ipl_name: "apa_v_mp_h_08_a",
        position: {
            X: -786.9469,
            Y: 315.5655,
            Z: 217.6383
        },
        category: "Online apartament"
    },

    {
        name: "Aqua 2 Apartment",
        ipl_name: "apa_v_mp_h_08_c",
        position: {
            X: -786.9756,
            Y: 315.723,
            Z: 187.9134
        },
        category: "Online apartament"
    },

    {
        name: "Aqua 3 Apartment",
        ipl_name: "apa_v_mp_h_08_b",
        position: {
            X: -774.0349,
            Y: 342.0296,
            Z: 196.6862
        },
        category: "Online apartament"
    },

    {
        name: "Executive Rich",
        ipl_name: "ex_dt1_02_office_02b",
        position: {
            X: -141.1987,
            Y: -620.913,
            Z: 168.8205
        },
        category: "Office"
    },

    {
        name: "Executive Cool",
        ipl_name: "ex_dt1_02_office_02c",
        position: {
            X: -141.5429,
            Y: -620.9524,
            Z: 168.8204
        },
        category: "Office"
    },

    {
        name: "Executive Contrast",
        ipl_name: "ex_dt1_02_office_02a",
        position: {
            X: -141.2896,
            Y: -620.9618,
            Z: 168.8204
        },
        category: "Office"
    },

    {
        name: "Old Spice Warm",
        ipl_name: "ex_dt1_02_office_01a",
        position: {
            X: -141.4966,
            Y: -620.8292,
            Z: 168.8204
        },
        category: "Office"
    },

    {
        name: " Old Spice Classical",
        ipl_name: "ex_dt1_02_office_01b",
        position: {
            X: -141.3997,
            Y: -620.9006,
            Z: 168.8204
        },
        category: "Office"
    },

    {
        name: "Old Spice Vintage",
        ipl_name: "ex_dt1_02_office_01c",
        position: {
            X: -141.5361,
            Y: -620.9186,
            Z: 168.8204
        },
        category: "Office"
    },

    {
        name: "Power Broker Ice",
        ipl_name: "ex_dt1_02_office_03a",
        position: {
            X: -141.392,
            Y: -621.0451,
            Z: 168.8204
        },
        category: "Office"
    },

    {
        name: "Power Broker Conservative",
        ipl_name: "ex_dt1_02_office_03b",
        position: {
            X: -141.1945,
            Y: -620.8729,
            Z: 168.8204
        },
        category: "Office"
    },

    {
        name: "Power Broker Polished",
        ipl_name: "ex_dt1_02_office_03c",
        position: {
            X: -141.4924,
            Y: -621.0035,
            Z: 168.8205
        },
        category: "Office"
    },

    {
        name: "Executive Rich",
        ipl_name: "ex_dt1_11_office_02b",
        position: {
            X: -75.8466,
            Y: -826.9893,
            Z: 243.3859
        },
        category: "Office"
    },

    {
        name: "Executive Cool",
        ipl_name: "ex_dt1_11_office_02c",
        position: {
            X: -75.49945,
            Y: -827.05,
            Z: 243.386
        },
        category: "Office"
    },

    {
        name: "Executive Contrast",
        ipl_name: "ex_dt1_11_office_02a",
        position: {
            X: -75.49827,
            Y: -827.1889,
            Z: 243.386
        },
        category: "Office"
    },

    {
        name: "Old Spice Warm",
        ipl_name: "ex_dt1_11_office_01a",
        position: {
            X: -75.44054,
            Y: -827.1487,
            Z: 243.3859
        },
        category: "Office"
    },

    {
        name: "Old Spice Classical",
        ipl_name: "ex_dt1_11_office_01b",
        position: {
            X: -75.63942,
            Y: -827.1022,
            Z: 243.3859
        },
        category: "Office"
    },

    {
        name: "Old Spice Vintage",
        ipl_name: "ex_dt1_11_office_01c",
        position: {
            X: -75.47446,
            Y: -827.2621,
            Z: 243.386
        },
        category: "Office"
    },

    {
        name: "Power Broker Ice",
        ipl_name: "ex_dt1_11_office_03a",
        position: {
            X: -75.56978,
            Y: -827.1152,
            Z: 243.3859
        },
        category: "Office"
    },

    {
        name: "Power Broker Conservative",
        ipl_name: "ex_dt1_11_office_03b",
        position: {
            X: -75.51953,
            Y: -827.0786,
            Z: 243.3859
        },
        category: "Office"
    },

    {
        name: "Power Broker Polished",
        ipl_name: "ex_dt1_11_office_03c",
        position: {
            X: -75.41915,
            Y: -827.1118,
            Z: 243.3858
        },
        category: "Office"
    },

    {
        name: "Executive Rich",
        ipl_name: "ex_sm_13_office_02b",
        position: {
            X: -1579.756,
            Y: -565.0661,
            Z: 108.523
        },
        category: "Office"
    },

    {
        name: "Executive Cool",
        ipl_name: "ex_sm_13_office_02c",
        position: {
            X: -1579.678,
            Y: -565.0034,
            Z: 108.5229
        },
        category: "Office"
    },

    {
        name: "Executive Contrast",
        ipl_name: "ex_sm_13_office_02a",
        position: {
            X: -1579.583,
            Y: -565.0399,
            Z: 108.5229
        },
        category: "Office"
    },

    {
        name: "Old Spice Warm",
        ipl_name: "ex_sm_13_office_01a",
        position: {
            X: -1579.702,
            Y: -565.0366,
            Z: 108.5229
        },
        category: "Office"
    },

    {
        name: "Old Spice Classical",
        ipl_name: "ex_sm_13_office_01b",
        position: {
            X: -1579.643,
            Y: -564.9685,
            Z: 108.5229
        },
        category: "Office"
    },

    {
        name: "Old Spice Vintage",
        ipl_name: "ex_sm_13_office_01c",
        position: {
            X: -1579.681,
            Y: -565.0003,
            Z: 108.523
        },
        category: "Office"
    },

    {
        name: "Power Broker Ice",
        ipl_name: "ex_sm_13_office_03a",
        position: {
            X: -1579.677,
            Y: -565.0689,
            Z: 108.5229
        },
        category: "Office"
    },

    {
        name: "Power Broker Conservative",
        ipl_name: "ex_sm_13_office_03b",
        position: {
            X: -1579.708,
            Y: -564.9634,
            Z: 108.5229
        },
        category: "Office"
    },

    {
        name: "Power Broker Polished",
        ipl_name: "ex_sm_13_office_03c",
        position: {
            X: -1579.693,
            Y: -564.8981,
            Z: 108.5229
        },
        category: "Office"
    },

    {
        name: "Executive Rich",
        ipl_name: "ex_sm_15_office_02b",
        position: {
            X: -1392.667,
            Y: -480.4736,
            Z: 72.04217
        },
        category: "Office"
    },

    {
        name: "Executive Cool",
        ipl_name: "ex_sm_15_office_02c",
        position: {
            X: -1392.542,
            Y: -480.4011,
            Z: 72.04211
        },
        category: "Office"
    },

    {
        name: "Executive Contrast",
        ipl_name: "ex_sm_15_office_02a",
        position: {
            X: -1392.626,
            Y: -480.4856,
            Z: 72.04212
        },
        category: "Office"
    },

    {
        name: "Old Spice Warm",
        ipl_name: "ex_sm_15_office_01a",
        position: {
            X: -1392.617,
            Y: -480.6363,
            Z: 72.04208
        },
        category: "Office"
    },

    {
        name: "Old Spice Classical",
        ipl_name: "ex_sm_15_office_01b",
        position: {
            X: -1392.532,
            Y: -480.7649,
            Z: 72.04207
        },
        category: "Office"
    },

    {
        name: "Old Spice Vintage",
        ipl_name: "ex_sm_15_office_01c",
        position: {
            X: -1392.611,
            Y: -480.5562,
            Z: 72.04214
        },
        category: "Office"
    },

    {
        name: "Power Broker Ice",
        ipl_name: "ex_sm_15_office_03a",
        position: {
            X: -1392.563,
            Y: -480.549,
            Z: 72.0421
        },
        category: "Office"
    },

    {
        name: "Power Broker Convservative",
        ipl_name: "ex_sm_15_office_03b",
        position: {
            X: -1392.528,
            Y: -480.475,
            Z: 72.04206
        },
        category: "Office"
    },

    {
        name: "Power Broker Polished",
        ipl_name: "ex_sm_15_office_03c",
        position: {
            X: -1392.416,
            Y: -480.7485,
            Z: 72.04207
        },
        category: "Office"
    },

    {
        name: "Clubhouse 1",
        ipl_name: "bkr_biker_interior_placement_interior_0_biker_dlc_int_01_milo",
        position: {
            X: 1107.04,
            Y: -3157.399,
            Z: -37.51859
        },
        category: "Clubhouse"
    },

    {
        name: "Clubhouse 2",
        ipl_name: "bkr_biker_interior_placement_interior_1_biker_dlc_int_02_milo",
        position: {
            X: 998.4809,
            Y: -3164.711,
            Z: -38.90733
        },
        category: "Clubhouse"
    },

    {
        name: "Warehouse 1",
        ipl_name: "bkr_biker_interior_placement_interior_2_biker_dlc_int_ware01_milo",
        position: {
            X: 1009.5,
            Y: -3196.6,
            Z: -38.99682
        },
        category: "Warehouse"
    },

    {
        name: "Warehouse 2",
        ipl_name: "bkr_biker_interior_placement_interior_3_biker_dlc_int_ware02_milo",
        position: {
            X: 1051.491,
            Y: -3196.536,
            Z: -39.14842
        },
        category: "Warehouse"
    },

    {
        name: "Warehouse 3",
        ipl_name: "bkr_biker_interior_placement_interior_4_biker_dlc_int_ware03_milo",
        position: {
            X: 1093.6,
            Y: -3196.6,
            Z: -38.99841
        },
        category: "Warehouse"
    },

    {
        name: "Warehouse 4",
        ipl_name: "bkr_biker_interior_placement_interior_5_biker_dlc_int_ware04_milo",
        position: {
            X: 1121.897,
            Y: -3195.338,
            Z: -40.4025
        },
        category: "Warehouse"
    },

    {
        name: "Warehouse 5",
        ipl_name: "bkr_biker_interior_placement_interior_6_biker_dlc_int_ware05_milo",
        position: {
            X: 1165,
            Y: -3196.6,
            Z: -39.01306
        },
        category: "Warehouse"
    },

    {
        name: "Warehouse Small",
        ipl_name: "ex_exec_warehouse_placement_interior_1_int_warehouse_s_dlc_milo",
        position: {
            X: 1094.988,
            Y: -3101.776,
            Z: -39.00363
        },
        category: "Company Warehouse"
    },

    {
        name: "Warehouse Medium",
        ipl_name: "ex_exec_warehouse_placement_interior_0_int_warehouse_m_dlc_milo",
        position: {
            X: 1056.486,
            Y: -3105.724,
            Z: -39.00439
        },
        category: "Company Warehouse"
    },

    {
        name: "Warehouse Large",
        ipl_name: "ex_exec_warehouse_placement_interior_2_int_warehouse_l_dlc_milo",
        position: {
            X: 1006.967,
            Y: -3102.079,
            Z: -39.0035
        },
        category: "Company Warehouse"
    },

    {
        name: "Cargarage",
        ipl_name: "imp_impexp_interior_placement_interior_1_impexp_intwaremed_milo_",
        position: {
            X: 994.5925,
            Y: -3002.594,
            Z: -39.64699
        },
        category: "Garage"
    },

    {
        name: "Normal Cargo Ship",
        ipl_name: "cargoship",
        position: {
            X: -163.3628,
            Y: -2385.161,
            Z: 5.999994
        },
        category: "Cargo Ship"
    },

    {
        name: "Sunken Cargo Ship",
        ipl_name: "sunkcargoship",
        position: {
            X: -163.3628,
            Y: -2385.161,
            Z: 5.999994
        },
        category: "Cargo Ship"
    },

    {
        name: "Burning Cargo Ship",
        ipl_name: "SUNK_SHIP_FIRE",
        position: {
            X: -163.3628,
            Y: -2385.161,
            Z: 5.999994
        },
        category: "Red Carpet"
    },

    {
        name: "Red Carpet",
        ipl_name: "redCarpet",
        position: {
            X: 300.5927,
            Y: 300.5927,
            Z: 104.3776
        },
        category: "Office"
    },

    {
        name: "Rekt Stilthouse Destroyed",
        ipl_name: "DES_StiltHouse_imapend",
        position: {
            X: -1020.518,
            Y: 663.27,
            Z: 153.5167
        },
        category: "Stilthouse"
    },

    {
        name: "Rekt Stilthouse Rebuild",
        ipl_name: "DES_stilthouse_rebuild",
        position: {
            X: -1020.518,
            Y: 663.27,
            Z: 153.5167
        },
        category: "Stilthouse"
    },

    {
        name: "Union Depository",
        ipl_name: "FINBANK",
        position: {
            X: 2.6968,
            Y: -667.0166,
            Z: 16.13061
        },
        category: "Bank"
    },

    {
        name: "Trevors Trailer Dirty",
        ipl_name: "TrevorsMP",
        position: {
            X: 1975.552,
            Y: 3820.538,
            Z: 33.44833
        },
        category: "Travor"
    },

    {
        name: "Trevors Trailer Clean",
        ipl_name: "TrevorsTrailerTidy",
        position: {
            X: 1975.552,
            Y: 3820.538,
            Z: 33.44833
        },
        category: "Travor"
    },

    {
        name: "Max Renda Shop",
        ipl_name: "refit_unload",
        position: {
            X: -585.8247,
            Y: -282.72,
            Z: 35.45475
        },
        category: "Store"
    },

    {
        name: "Jewel Store",
        ipl_name: "post_hiest_unload",
        position: {
            X: -630.07,
            Y: -236.332,
            Z: 38.05704
        },
        category: "Store"
    },

    {
        name: "FIB Lobby",
        ipl_name: "FIBlobby",
        position: {
            X: 110.4,
            Y: -744.2,
            Z: 45.7496
        },
        category: "FIB"
    },

    {
        name: "Carwash",
        ipl_name: "Carwash_with_spinners",
        position: {
            X: 55.7,
            Y: -1391.3,
            Z: 30.5
        },
        category: "Gta World"
    },

    {
        name: "Stadium",
        ipl_name: "sp1_10_real_interior_lod",
        position: {
            X: -248.491,
            Y: -2010.509,
            Z: 34.574
        },
        category: "Gta World"
    },

    {
        name: "Garage in La Mesa",
        ipl_name: "bkr_bi_id1_23_door",
        position: {
            X: 970.27453,
            Y: -1826.56982,
            Z: 31.11477
        },
        category: "Gta World"
    },

    {
        name: "Red Hill Valley church - Grave",
        ipl_name: "lr_cs6_08_grave_closed",
        position: {
            X: -282.4638,
            Y: 2835.84,
            Z: 55.914
        },
        category: "Gta World"
    },

    {
        name: "Lost's trailer park",
        ipl_name: "methtrailer_grp1",
        position: {
            X: 49.49379,
            Y: 3744.472,
            Z: 46.38629
        },
        category: "Gta World"
    },

    {
        name: "Lost Clubhouse",
        ipl_name: "bkr_bi_hw1_13_int",
        position: {
            X: 984.1552,
            Y: -95.3662,
            Z: 74.50
        },
        category: "Gta World"
    },

    {
        name: "Pillbox hospital",
        ipl_name: "rc12b_default",
        position: {
            X: 307.1680,
            Y: -590.807,
            Z: 43.280
        },
        category: "Gta World"
    },

    {
        name: "PDM (Simons Car Dealer)",
        ipl_name: "shr_int",
        position: {
            X: -54.30,
            Y: -1109.3767,
            Z: 26.4358
        },
        category: "Gta World"
    },

    {
        name: "Remove Zancudoe Gates",
        ipl_name: "CS3_07_MPGates",
        position: {
            X: -1599.95,
            Y: 2807.05,
            Z: 17.204
        },
        category: "Gta World"
    },

    {
        name: "Ferris Wheel",
        ipl_name: "ferris_finale_Anim",
        position: {
            X: -1645.55,
            Y: -1113.04,
            Z: 12.65
        },
        category: "Gta World"
    },

    {
        name: "Casino Penthouse Glassfront",
        ipl_name: "hei_dlc_windows_casino",
        position: {
            X: 968.156,
            Y: 0.3060,
            Z: 111.2922
        },
        category: "Gta World"
    },

    {
        name: "Golfflags",
        ipl_name: "golfflags",
        position: {
            X: -1032,
            Y: -84,
            Z: 44
        },
        category: "Gta World"
    },

    {
        name: "Racetrack",
        ipl_name: "racetrack01",
        position: {
            X: 1978,
            Y: 3111,
            Z: 46
        },
        category: "Gta World"
    },

    {
        name: "Gunrunning Heist Yacht",
        ipl_name: [
            "gr_heist_yacht2",
            "gr_heist_yacht2_bar",
            "gr_heist_yacht2_bedrm",
            "gr_heist_yacht2_bridge",
            "gr_heist_yacht2_enginrm",
            "gr_heist_yacht2_loungesmboat"
        ],
        position: {
            X: -1418.21000000,
            Y: 6749.81000000,
            Z: 10.98968000
        },
        category: "Multiple ipl"
    },

    {
        name: "Dignity Heist Yacht",
        ipl_name: [
            "smboat_lod",
            "hei_yacht_heist",
            "hei_yacht_heist_enginrm",
            "hei_yacht_heist_Lounge",
            "hei_yacht_heist_Bridge",
            "hei_yacht_heist_Bar",
            "hei_yacht_heist_Bedrm",
            "hei_yacht_heist_DistantLights",
            "hei_yacht_heist_LODLightssmboat"
        ],
        position: {
            X: -2027.946,
            Y: -1036.695,
            Z: 6.707587
        },
        category: "Multiple ipl"
    },

    {
        name: "Dignity Party Yacht",
        ipl_name: [
            "smboat_lod",
            "hei_yacht_heist",
            "hei_yacht_heist_enginrm",
            "hei_yacht_heist_Lounge",
            "hei_yacht_heist_Bridge",
            "hei_yacht_heist_Bar",
            "hei_yacht_heist_Bedrm",
            "hei_yacht_heist_DistantLights",
            "hei_yacht_heist_LODLightssmboat"
        ],
        position: {
            X: -2023.643,
            Y: -1038.119,
            Z: 5.576781
        },
        category: "Multiple ipl"
    },

    {
        name: "Aircraft Carrier",
        ipl_name: [
            "hei_carrier_DistantLights",
            "hei_Carrier_int1",
            "hei_Carrier_int2",
            "hei_Carrier_int3",
            "hei_Carrier_int4",
            "hei_Carrier_int5",
            "hei_Carrier_int6",
            "hei_carrier_LODLights",
        ],
        position: {
            X: 3084.73,
            Y: -4770.709,
            Z: 15.26167
        },
        category: "Multiple ipl"
    },

    {
        name: "Bridge Train Crash",
        ipl_name: [
            "canyonriver01_traincrash",
            "railing_end"
        ],
        position: {
            X: 532.1309,
            Y: 4526.187,
            Z: 89.79387
        },
        category: "Multiple ipl"
    },

    {
        name: "Bridge Train Normal",
        ipl_name: [
            "canyonriver01",
            "railing_start"
        ],
        category: "Multiple ipl"
    },

    {
        name: "North Yankton",
        ipl_name: [
            "prologue01",
            "prologue01c",
            "prologue01d",
            "prologue01e",
            "prologue01f",
            "prologue01g",
            "prologue01h",
            "prologue01i",
            "prologue01j",
            "prologue01k",
            "prologue01z",
            "prologue02",
            "prologue03",
            "prologue03b",
            "prologue03_grv_dug",
            "prologue_grv_torch",
            "prologue04",
            "prologue04b",
            "prologue04_cover",
            "des_protree_end",
            "des_protree_start",
            "prologue05",
            "prologue05b",
            "prologue06",
            "prologue06b",
            "prologue06_int",
            "prologue06_pannel",
            "plg_occl_00",
            "prologue_occl",
            "prologuerd",
            "prologuerdb"
        ],
        position: {
            X: 3217.697,
            Y: -4834.826,
            Z: 111.8152
        },
        category: "Multiple ipl"
    },

    {
        name: "ONeils Farm Burnt",
        ipl_name: [
            "farmint",
            "farm_burnt",
            "farm_burnt_props",
            "des_farmhouse",
            "des_farmhs_endimap",
            "des_farmhs_end_occl"
        ],
        position: {
            X: 2469.03,
            Y: 4955.278,
            Z: 45.11892
        },
        category: "Multiple ipl"
    },

    {
        name: "ONeils Farm",
        ipl_name: [
            "farm",
            "farm_lod",
            "farm_props",
            "farm_int",
        ],
        position: {
            X: 2469.03,
            Y: 4955.278,
            Z: 45.11892
        },

        category: "Multiple ipl"
    },

    {
        name: "Morgue",
        ipl_name: [
            "coronertrash",
            "Coroner_Int_On"
        ],
        position: {
            X: 275.446,
            Y: -1361.11,
            Z: 24.5378
        },
        category: "Multiple ipl"
    },

    {
        name: "Lester's factory",
        ipl_name: [
            "id2_14_during_door",
            "id2_14_during1",
        ],
        position: {
            X: 716.84,
            Y: -962.05,
            Z: 31.59
        },
        category: "Multiple ipl"
    },

    {
        name: "Life Invader Lobby ",
        ipl_name: [
            "facelobby",
            "facelobby_lod",
        ],
        position: {
            X: -1047.9,
            Y: -233.0,
            Z: 39.0
        },
        category: "Multiple ipl"
    },

    {
        name: "Tunnels",
        ipl_name: [
            "v_tunnel_hole",
            "v_tunnel_hole_lod"
        ],
        position: {
            X: -49.415,
            Y: -558.287,
            Z: 30.10
        },
        category: "Multiple ipl"
    },

    {
        name: "vw_dlc_casino_door",
        position: {
            X: 924.369,
            Y: 47.037,
            Z: 81.093
        },
        category: "Casino | NO IPL"
    },

    {
        name: "hei_dlc_casino_door",
        position: {
            X: 924.369,
            Y: 47.037,
            Z: 81.093
        },
        category: "Casino | NO IPL"
    },

    {
        name: "2 Car Garage",
        position: {
            X: 173.2903,
            Y: -1003.6,
            Z: -99.65707
        },
        category: "Car Garages | NO IPL"
    },

    {
        name: "6 Car Garage",
        position: {
            X: 197.8153,
            Y: -1002.293,
            Z: -99.65749
        },
        category: "Car Garages | NO IPL"
    },

    {
        name: "10 Car Garage",
        position: {
            X: 229.9559,
            Y: -981.7928,
            Z: -99.66071
        },
        category: "Car Garages | NO IPL"
    },

    {
        name: "Low End Apartment",
        position: {
            X: 261.4586,
            Y: -998.8196,
            Z: -99.00863
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "4 Integrity Way, Apt 28",
        position: {
            X: -18.07856,
            Y: -583.6725,
            Z: 79.46569
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "4 Integrity Way, Apt 30",
        position: {
            X: -35.31277,
            Y: -580.4199,
            Z: 88.71221
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "Dell Perro Heights, Apt 4",
        position: {
            X: -1468.14,
            Y: -541.815,
            Z: 73.4442
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "Dell Perro Heights, Apt 7",
        position: {
            X: -1477.14,
            Y: -538.7499,
            Z: 55.5264
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "Richard Majestic, Apt 2",
        position: {
            X: -915.811,
            Y: -379.432,
            Z: 113.6748
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "Tinsel Towers, Apt 42",
        position: {
            X: -614.86,
            Y: 40.6783,
            Z: 97.60007
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "EclipseTowers, Apt 3",
        position: {
            X: -773.407,
            Y: 341.766,
            Z: 211.397
        },
        category: "Apartments | NO IPL"
    },

    {
        name: " 3655 Wild Oats Drive",
        position: {
            X: -169.286,
            Y: 486.4938,
            Z: 137.4436
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "2044 North Conker Avenue",
        position: {
            X: 340.9412,
            Y: 437.1798,
            Z: 149.3925
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "2045 North Conker Avenue",
        position: {
            X: 373.023,
            Y: 416.105,
            Z: 145.7006
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "2862 Hillcrest Avenue",
        position: {
            X: -676.127,
            Y: 588.612,
            Z: 145.1698
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "2868 Hillcrest Avenue",
        position: {
            X: -763.107,
            Y: 615.906,
            Z: 144.1401
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "2874 Hillcrest Avenue",
        position: {
            X: -857.798,
            Y: 682.563,
            Z: 152.6529
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "2677 Whispymound Drive",
        position: {
            X: 120.5,
            Y: 549.952,
            Z: 184.097
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "2133 Mad Wayne Thunder",
        position: {
            X: -1288,
            Y: 440.748,
            Z: 97.69459
        },
        category: "Apartments | NO IPL"
    },

    {
        name: "Bunker Interior",
        position: {
            X: 899.5518,
            Y: -3246.038,
            Z: -98.04907
        },
        category: "Misc NO IPL"
    },

    {
        name: "CharCreator",
        position: {
            X: 402.5164,
            Y: -1002.847,
            Z: -99.2587
        },
        category: "Misc NO IPL"
    },

    {
        name: "Mission Carpark",
        position: {
            X: 405.9228,
            Y: -954.1149,
            Z: -99.6627
        },
        category: "Misc NO IPL"
    },

    {
        name: "Torture Room",
        position: {
            X: 136.5146,
            Y: -2203.149,
            Z: 7.30914
        },
        category: "Misc NO IPL"
    },

    {
        name: "Solomon's Office",
        position: {
            X: -1005.84,
            Y: -478.92,
            Z: 50.02733
        },
        category: "Misc NO IPL"
    },

    {
        name: "Psychiatrist's Office",
        position: {
            X: -1908.024,
            Y: -573.4244,
            Z: 19.09722
        },
        category: "Misc NO IPL"
    },

    {
        name: "Omega's Garage",
        position: {
            X: 2331.344,
            Y: 2574.073,
            Z: 46.68137
        },
        category: "Misc NO IPL"
    },

    {
        name: "Movie Theatre",
        position: {
            X: -1427.299,
            Y: -245.1012,
            Z: 16.8039
        },
        category: "Misc NO IPL"
    },

    {
        name: "Motel",
        position: {
            X: 152.2605,
            Y: -1004.471,
            Z: -98.99999
        },
        category: "Misc NO IPL"
    },

    {
        name: "Mandrazos Ranch",
        position: {
            X: 152.2605,
            Y: 1146.954,
            Z: 114.337
        },
        category: "Misc NO IPL"
    },

    {
        name: "Life Invader Office",
        position: {
            X: -1044.193,
            Y: -236.9535,
            Z: 37.96496
        },
        category: "Misc NO IPL"
    },

    {
        name: "Lester's House",
        position: {
            X: 1273.9,
            Y: -1719.305,
            Z: 54.77141
        },
        category: "Misc NO IPL"
    },

    {
        name: "FBI Top Floor",
        position: {
            X: 134.5835,
            Y: -749.339,
            Z: 258.152
        },
        category: "Misc NO IPL"
    },

    {
        name: "FBI Floor 47",
        position: {
            X: 134.5835,
            Y: -766.486,
            Z: 234.152
        },
        category: "Misc NO IPL"
    },

    {
        name: "FBI Floor 49",
        position: {
            X: 134.635,
            Y: -765.831,
            Z: 242.152
        },
        category: "Misc NO IPL"
    },

    {
        name: "IAA Office",
        position: {
            X: 117.22,
            Y: -620.938,
            Z: 206.1398
        },
        category: "Misc NO IPL"
    },

]