import * as alt from 'alt';
import * as native from 'natives';
import game from 'natives'

let webview;
alt.onServer('openInterior', () => {
    if (!webview) {
        webview = new alt.WebView('http://resource/client/html/index.html');
        webview.on('closeWebview', () => {
            alt.showCursor(false);
            webview.destroy();
            webview = undefined;
            
            alt.setInterval(() => {
                native.enableControlAction(2, 37, true);
                native.enableControlAction(2, 50, true);
                native.enableControlAction(0, 31, true);
                native.enableControlAction(0, 268, true);
                native.enableControlAction(0, 33, true);
                native.enableControlAction(0, 1, true);
                native.enableControlAction(0, 2, true);
                native.enableControlAction(0, 3, true);
                native.enableControlAction(0, 5, true);
                native.enableControlAction(0, 24, true);
            }, 0);
        });
        
        webview.focus();
        alt.showCursor(true);

        alt.setInterval(() => {
            native.disableControlAction(2, 37, true); // tab
            native.disableControlAction(2, 50, true); // scroll down
            native.disableControlAction(0, 31, true); // w
            native.disableControlAction(0, 268, true); // s
            native.disableControlAction(0, 34, true); // a
            native.disableControlAction(0, 1, true); // Lright
            native.disableControlAction(0, 2, true); // Ldown
            native.disableControlAction(0, 5, true); // Lleft
            native.disableControlAction(0, 3, true); // Lup
            native.disableControlAction(0, 24, true); // mouse left
        }, 0);
    }
    webview.on('joinInterior', (ipl_name, PositionX, PositionY, PositionZ) => {

        let ipl2 = ipl_name.split(',', ipl_name.length);
        ipl2.forEach(ipl => {
            console.log("ipl" + ipl);
            game.requestIpl(ipl);
            
            // Uncomment below code to enable remove ipl
            // alt.setTimeout(() => { 
            //     game.removeIpl(ipl);
            //  }, timeMS);
        });



        let pos = new alt.Vector3(PositionX, PositionY, PositionZ)
        alt.emitServer('interiorPos', pos);
    });



});
