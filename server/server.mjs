import * as alt from 'alt';
import chat from 'chat';

chat.registerCmd('interior', (player) => {
    alt.emitClient(player, 'openInterior');
});

alt.onClient('interiorPos', (player, pos) => {
    player.pos = pos;
});
