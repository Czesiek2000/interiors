# What is it ? 
With this resource you can easily teleport to different interiors from GTAV and GTA:Online 

## Instalation 
* Download all files from repo and put it inside you server resource folder
* Add in your `server.cfg` file inside resource array `interiors`
* Everything is ready to use 

### How to use it 
Inside `server.js` file inside server folder is located chat command that opens interior selection. You can change this command inside this file. By default the command is `interior` so inside chat type `/interior` to open interior selection

# Screenshot from game
> Some screenshot presents how this resource looks in game

![command](docs/cmd.png)
> Command to open the interior menu

![menu](docs/menu.png)
> This is how menu open looks like

![interior](docs/interior.png)
> When you click a button you will be teleported to the interior

# Note
This resource automaticaly doesn't remove ipl from your game cash. So after you reconnect with game console it won't disappear. 

You can enable function to remove ipl after amount of time. Just uncomment in `client` file commented code and replace `timeMS` with timeout that need to pass after interior will be removed

Look inside code for similar code
```javascript
// alt.setTimeout(() => { 
//     game.removeIpl(ipl);
// }, timeMS);
```

# Licence 
This repo is under MIT licence. You can use it however you want